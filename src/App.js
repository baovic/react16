// import React, { Component } from 'react'
// import { Link } from "@reach/router";

// export default props => (
//     <div className="app">   
//       <Link to="/">Home</Link>
//       <Link to="about">About</Link>
//       {props.children}
//     </div>
// )
// ----------------------
import React, { Component } from 'react';
import { Link } from "@reach/router";
import Header from './components/Layout/Head'

// import Footer from './layout/Footer'
// import Slider from './layout/Slider'
// import MenuLeft from './layout/MenuLeft'

class App extends Component {
  render () {
    return (
      <div>
        <Header />

        {/* <Slider /> */}

        <section>
          <div className="container">
            <div className="row">
              {/* <MenuLeft /> */}
              {this.props.children}
            </div>
          </div>
        </section>

        {/* <Footer /> */}
      </div>
    )
  }
}
export default App
