import React from 'react';
import ReactDOM from 'react-dom';
import { Router } from "@reach/router";


import './index.css';
import App from './App';
import Home from './components/Home';
import Account from './components/Account/Index';
import Login from './components/Member/Login';

import * as serviceWorker from './serviceWorker';

const NotFound = () => <p>Sorry, nothing here</p>

ReactDOM.render(
<div>
    <Router>
      <App path="/">
        <Home path="/" />
        <Account path="/account" />
        <Login path="/login" />
        
        <NotFound default />
      </App>
      
    </Router>
</div>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
